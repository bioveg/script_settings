# Settings and Scripts and other important information

## Production environment

### Access to servers

**Virtualization**
> OS: `Proxmox`
>
> Address: https://172.16.1.43:8006
>
> UserName: `root`
>
> Password: `lol123`
>
> VM ids: 
> >`101:H6-Firewall`
>> 
> >`20010:H6-docker1`

**Firewall** 
> OS: `opnsense`
>
> WAN IP: `172.16.1.19`
>
> LAN IP: `192.168.2.1`
>
> UserName: `root`
>
> Password: `Cisco1234`

**Prod1**
> OS: `Ubuntu`
>
> LAN IP: `192.168.10.30`
>
> Users: `mka`, `cdi`, `mark`
>
> Access: `ssh $user@172.16.1.19 -p 5000`

**pgSQL**
> Access: http://172.16.1.19:8080
>
> AccessProd: `http://pgsql`
>
> UserName: `root`
>
> Password: `lol123`
>
> DB: `root`

**InfluxDB**
> access http://172.16.1.19:8086
>
> Access prod: `http://influx`
>
> UserName: `root`
>
> Password: `Cisco1234`
>
> org: `bioveg`
>
> Bucket prod: `sensor`
>
> Bucket test: `sensor_tets`

**Basestation**
> SSID: `BIOVEG_basestation`
>
> Pass: `h6bioveg`
>
> Address: `10.10.10.1`
>
> Subnet: `255.255.255.0`

**Backend**
> baseaddress: https://wgniash.duckdns.org:15000

**Frontend(www)**
> Address: https://bioveg.niash.dk

**RaspberryPi_Dashboard**
> Wireguard priavte key: `MI13bwM37iaoauqRa6l3x7Gtt6T9QjIKh3WxY0QbVVU=`

